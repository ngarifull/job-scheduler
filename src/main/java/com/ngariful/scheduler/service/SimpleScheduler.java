package com.ngariful.scheduler.service;

import com.ngariful.scheduler.model.SimpleJob;
import com.ngariful.scheduler.repository.SimpleJobRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleScheduler {
    private final RabbitTemplate rabbitTemplate;
    private final SimpleJobRepository simpleJobRepository;

    @Value("${rabbit.exchange}")
    public String EXCHANGE_NAME;

    @Value("${rabbit.routing.key}")
    public String ROUTING_KEY;

    @Scheduled(fixedDelay = 60000)
    public void runJobs() {
        List<SimpleJob> simpleJobList = simpleJobRepository.findAll().stream()
                .filter(cc -> checkCron(cc.getCron()))
                .collect(Collectors.toList());

        simpleJobList.forEach(this::postRabbit);
    }

    private void postRabbit(SimpleJob simpleJob) {
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, ROUTING_KEY, simpleJob.getCode());
        log.info(String.format("Posting to exchange : %s, with routing key %s, message with job key %s",
                EXCHANGE_NAME, ROUTING_KEY, simpleJob.getCode()));
    }


    public boolean checkCron(String cron) {
        LocalDateTime next = getNextExecutionTime(cron);
        LocalDateTime now = LocalDateTime.now().withSecond(0).withNano(0);

        return now.equals(next);
    }

    private LocalDateTime getNextExecutionTime(String cron) {
        CronSequenceGenerator cronTrigger = new CronSequenceGenerator(cron);
        Date nextDate = cronTrigger.next(new Date());
        return nextDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
                .withSecond(0)
                .withNano(0);
    }
}
