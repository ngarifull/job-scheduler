package com.ngariful.scheduler.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name ="job")
public class SimpleJob {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private String code;
    private String cron;
    private Integer executionTime;
    private Boolean oncePerDay;
    private Byte isDeleted;
}
