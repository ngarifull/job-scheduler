package com.ngariful.scheduler.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "job_history")
public class SimpleJobHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long jobId;
}
