package com.ngariful.scheduler.repository;

import com.ngariful.scheduler.model.SimpleJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimpleJobRepository extends JpaRepository<SimpleJob, Long> {
    @Query("SELECT sj FROM SimpleJob sj LEFT JOIN SimpleJobHistory sh ON sj.id = sh.jobId WHERE sj.isDeleted = 0")
    List<SimpleJob> findAll();
}
